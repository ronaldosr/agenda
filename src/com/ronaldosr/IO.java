package com.ronaldosr;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO {
    public static void imprimirTituloJogo() {
        System.out.println("*** Agenda Telefônica *** ");
    }

    public static String lerOpcaoMenuPrincipal() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nInforme a opção desejada " +
                "\n(1) - Incluir contato " +
                "\n(2) - Procurar contato por e-mail " +
                "\n(3) - Procurar contato por telefone " +
                "\n(4) - Remover contato por e-mail " +
                "\n(5) - Sair");
        return scanner.nextLine();
    }

    public static int lerQuantidadeContatosParaCadastro() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nQuantos contatos você deseja cadastrar?");
        return scanner.nextInt();
    }

    public static Map<String, String> lerDadosContato(int posicao) {
        Map<String, String> dados = new HashMap<>();

        System.out.println("\n" + posicao + "º contato......");
        dados.put("nome", lerNomeContato());
        dados.put("telefone", lerTelefoneContato());
        dados.put("email", lerEmailContato());

        return dados;
    }

    public static String lerNomeContato() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe o nome do contato");
        return scanner.nextLine();
    }

    public static String lerTelefoneContato() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe o telefone do contato");
        return scanner.nextLine();
    }

    public static String lerEmailContato() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe o e-mail do contato");
        return scanner.nextLine();
    }

    public static void imprimirMensagemCadastroComSucesso() {
        System.out.println("\nContato(s) cadastrado(s) com sucesso!");
    }

    public static void imprimirMensagemContatoEncontrado(int i) {
        System.out.println("\n" + i + " contato(s) encontrado(s)" +
                "\n--------------------------------");
    }

    public static void imprimirContato(Contato contato) {
        System.out.println(
                "Nome: " + contato.getNome() +
                "\nTelefone: " + contato.getTelefone() +
                "\nE-mail: " + contato.getEmail() + "\n");
    }

    public static void imprimirOpcaoInvalida() {
        System.out.println("Opçao inválida! \n");
    }

    public static void imprimirErroQtdContatos() {
        System.out.println("Você deve cadastrar ao menos um contato! \n");
    }

    public static String lerConfirmacaoExlcusao(int qtdContatos) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Tem certeza que deseja excluir " + qtdContatos + " contato(s)? Pressione qualquer tecla para cancelar ou digite 'SIM' para confirmar.");
        return  scanner.nextLine();
    }

    public static void imprimirMensagemConfirmacaoExclusaoCancelada() {
        System.out.println("Exclusão cancelada! \n");
    }

    public static void imprimirMensagemSaida() {
        System.out.println("Bye... \n");
    }

    public static void imprimirMensagemExclusaoComSucesso() {
        System.out.println("\nContato(s) excluído(s) com sucesso!");
    }

}
