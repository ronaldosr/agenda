package com.ronaldosr;

import java.util.List;

public class Menu {

    private boolean continua;

    public Menu() {
        Agenda agenda = new Agenda();
        IO.imprimirTituloJogo();

        setContinua(true);
        String opcao;

        while (continua) {
            opcao = IO.lerOpcaoMenuPrincipal();

            if (opcao.equalsIgnoreCase("1")) {
                this.menuIncluirContato(agenda);

            } else if (opcao.equalsIgnoreCase("2")) {
                this.menuProcurarContatoPorEmail(agenda);

            } else if (opcao.equalsIgnoreCase("3")) {
                this.menuProcurarContatoPorTelefone(agenda);

            } else if (opcao.equalsIgnoreCase("4")) {
                this.menuRemoverContatoPorEmail(agenda);

            } else if (opcao.equalsIgnoreCase("5")) {
                this.sairAgenda();

            } else {
                IO.imprimirOpcaoInvalida();
            }
        }
    }

    private void menuIncluirContato(Agenda agenda) {
        int qtdContatos = IO.lerQuantidadeContatosParaCadastro();

        if (qtdContatos <= 0) {
            IO.imprimirErroQtdContatos();
        } else {
            agenda.setContatos(agenda.cadastrarContatos(agenda.getContatos(), qtdContatos));
        }
    }

    private void menuProcurarContatoPorEmail(Agenda agenda) {
        String email = IO.lerEmailContato();
        List<Contato> contatosEncontrados = agenda.procuraContatosPorEmail(agenda.getContatos(), email);
        agenda.imprimirContatos(contatosEncontrados);
    }

    private void menuProcurarContatoPorTelefone(Agenda agenda) {
        String telefone = IO.lerTelefoneContato();
        List<Contato> contatosEncontrados = agenda.procuraContatosPorTelefone(agenda.getContatos(), telefone);
        agenda.imprimirContatos(contatosEncontrados);
    }

    private void menuRemoverContatoPorEmail(Agenda agenda) {
        String email = IO.lerEmailContato();
        List<Contato> contatosEncontrados = agenda.procuraContatosPorEmail(agenda.getContatos(), email);

        if (contatosEncontrados.size() == 0) {
            IO.imprimirMensagemContatoEncontrado(contatosEncontrados.size());
        } else {
            agenda.imprimirContatos(contatosEncontrados);
            String confirmaExclusao = IO.lerConfirmacaoExlcusao(contatosEncontrados.size());

            if (confirmaExclusao.equalsIgnoreCase("SIM")) {
                for (Contato contato : contatosEncontrados) {
                    agenda.removerContatoLista(agenda.getContatos(), contato);
                }
                IO.imprimirMensagemExclusaoComSucesso();
            } else {
                IO. imprimirMensagemConfirmacaoExclusaoCancelada();
            }
        }
    }

    private void sairAgenda() {
        IO.imprimirMensagemSaida();
        setContinua(false);
    }

    public boolean isContinua() {
        return continua;
    }

    public void setContinua(boolean continua) {
        this.continua = continua;
    }
}
