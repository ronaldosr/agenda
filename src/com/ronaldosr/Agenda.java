package com.ronaldosr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Agenda {

    private List<Contato> contatos;

    public List<Contato> adicionarContatoLista (List<Contato> contatos, Contato contato) {
        contatos.add(contato);
        return contatos;
    }

    public void removerContatoLista (List<Contato> contatos, Contato contato) {
        contatos.remove(contato);
    }

    public List<Contato> procuraContatosPorEmail (List<Contato> contatos, String email) {
        List<Contato> contatosEncontrados = new ArrayList<>();
        for (Contato contato : contatos) {
            if (contato.getEmail().equalsIgnoreCase(email)) {
                contatosEncontrados.add(contato);
            }
        }
        return contatosEncontrados;
    };

    public List<Contato> procuraContatosPorTelefone(List<Contato> contatos, String telefone) {
        List<Contato> contatosEncontrados = new ArrayList<>();
        for (Contato contato : contatos) {
            if (contato.getTelefone().equalsIgnoreCase(telefone)) {
                contatosEncontrados.add(contato);
            }
        }
        return contatosEncontrados;
    }

    public List<Contato> cadastrarContatos(List<Contato> contatos, int qtdContatos) {

        if (contatos == null) {
            List<Contato> novosContatos = new ArrayList<>();
            contatos = novosContatos;
        }

        for (int i = 0; i < qtdContatos; i++) {
            Map<String, String> dados = IO.lerDadosContato(i+1);
            Contato contato = new Contato();
            contato.setNome(dados.get("nome"));
            contato.setTelefone(dados.get("telefone"));
            contato.setEmail(dados.get("email"));
            contatos.add(contato);
        }
        IO.imprimirMensagemCadastroComSucesso();
        return contatos;
    }

    public void imprimirContatos(List<Contato> contatosEncontrados) {
        if (contatosEncontrados == null) {
            IO.imprimirMensagemContatoEncontrado(0);
        } else {
            IO.imprimirMensagemContatoEncontrado(contatosEncontrados.size());
            for (Contato contato : contatosEncontrados) {
                IO.imprimirContato(contato);
            }
        }
    }


    public Agenda() {
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contato> contatos) {
        this.contatos = contatos;
    }
}
